# Oriol Sanchez
# 09/03/2021

# Mou la posicio
def moviment(x, y, tauler):
    if x <= 9 and y <= 9:
        if tauler[x+1][y] is False:
            x = x + 1
        elif tauler[x][y+1] is False:
            y = y + 1
        elif tauler[x][y-1] is False:
            y = y - 1
        elif x == 10 and y == 5:
            return
        elif tauler[x-1][y] is False:
            x = x - 1
    else:
        return

    tauler[x][y] = "a"
    moviment(x, y, tauler)


# Crea tauler i mou posicio
def laberint():
    tauler = [[True, False, True, True, True, True, True, True, True, True, True],
              [True, False, False, False, True, False, False, True, False, False, True],
              [True, True, True, False, True, True, False, True, False, True, True],
              [True, False, False, False, True, False, False, True, False, True, True],
              [True, True, True, False, True, True, False, True, False, False, True],
              [True, False, False, False, True, True, False, False, False, False, True],
              [True, True, True, False, True, False, True, True, True, False, True],
              [True, False, True, False, False, True, False, False, True, False, True],
              [True, False, True, True, False, False, True, False, False, False, True],
              [True, False, False, False, True, False, True, False, True, True, True],
              [True, True, True, True, True, False, True, True, True, True, True]]

    x = 0
    y = 1
    posicio = tauler[x][y]
    posicioguanyadora = False

    for columna in range(len(tauler)):
        llistafila = []
        for fila in range(len(tauler)):
            llistafila.append(tauler[columna][fila])
        print(llistafila)

    print("\n")

    while not posicioguanyadora:
        if posicio == tauler[10][5]:
            moviment(x, y, tauler)
            posicioguanyadora = True
        print("Has resolt el laberint!")

    for columna in range(len(tauler)):
        llistafila = []
        for fila in range(len(tauler)):
            llistafila.append(tauler[columna][fila])
        print(llistafila)

    print("\n")
    print("Aquest és el recorregut fet!")


# Main
laberint()
